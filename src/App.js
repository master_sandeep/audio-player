import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Audio from './Audio'
import './App.css';

function App() {
  return (
    <div className="App">
      <Audio />
    </div>
  );
}

export default App;
