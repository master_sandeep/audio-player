import React from 'react'
import { Button, FormControl, InputGroup } from 'react-bootstrap';

export default class Audio extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            isPlaying: false,
            customPlayTime: '',
            isError: ''
        }
    };

    handleForword = () => {
        this.audio.currentTime = this.audio.currentTime + 5;
    }

    handleRewind = () => {
        this.audio.currentTime = this.audio.currentTime - 5;
    }

    handleCustomPlayTime = (e) => {
        e.preventDefault();
        this.setState({ customPlayTime: e.target.value })
    }

    handleCustomPlayTimeSubmit = () => {
        if (this.state.customPlayTime == '') {
            this.setState({ isError: true })
            return;
        }
        this.audio.currentTime = this.state.customPlayTime;
        this.setState({ customPlayTime: '', isError: false })
        if (!this.state.isPlaying) {
            this.setState({ isPlaying: true })
            this.audio.play();
        }
    }

    componentDidMount() {
        this.audio.onpause = () => {
            this.setState({ isPlaying: false })
        };
    }

    render() {
        const src = "https://mp3.gisher.org/download/1000/preview/true";

        return (
            <div className='center'>
                <audio className='audioPlay' ref={(audio) => { this.audio = audio }} src={src} controls controlslist="nodownload" />
                <div className='mt-2 mb-2'>
                    <Button variant="primary" size="sm" onClick={this.handleRewind}>
                        Rewind
                    </Button>
                    <Button variant="primary" className='ml-2' size="sm" onClick={this.handleForword}>
                        Forword
                    </Button>
                </div>
                <div>
                    <InputGroup className="mb-6">
                        <FormControl
                            placeholder="Enter time in seconds"
                            onChange={this.handleCustomPlayTime}
                            type='number'
                            value={this.state.customPlayTime}
                            required
                        />
                        <InputGroup.Append>
                            <Button variant="primary" size="sm" className='ml-2' onClick={this.handleCustomPlayTimeSubmit}>
                                Play from here
                            </Button>
                        </InputGroup.Append>
                    </InputGroup>
                    {this.state.isError && <label className='text-danger mt-1'>is required</label>}
                </div>
            </div>
        );
    }
}